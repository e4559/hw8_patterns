"use strict";

const root = document.getElementById("root");

const section = document.createElement("section");
section.className = "join-our-program-container";

const joinProgram = document.createElement("div");
joinProgram.className = "join-our-program";

let firstContainer = root.appendChild(section).appendChild(joinProgram);
const joinDiv = document.createElement("div");
joinDiv.className = "join-h2";

const joinH2 = document.createElement("h3");
joinH2.innerText = "Join Our Program";
joinDiv.appendChild(joinH2);

const joinPContainer = document.createElement("div");
joinPContainer.className = "join-p";
const joinP = document.createElement("p");
joinP.innerText =
  "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

joinPContainer.appendChild(joinP);

const form = document.createElement("form");
const input = document.createElement("input");
input.type = "email";
input.placeholder = "Email";
const button = document.createElement("button");
button.innerText = "SUBSCRIBE";
form.appendChild(input);
form.appendChild(button);

firstContainer.appendChild(joinDiv);
firstContainer.appendChild(joinPContainer);
firstContainer.appendChild(form);

let text;

function writeText(e) {
  text = e.target.value;
}

function logInput(e) {
  e.preventDefault();
  console.log(text);
}

input.addEventListener("keyup", (e) => writeText(e));
button.addEventListener("click", (e) => logInput(e));
